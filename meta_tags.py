import pandas as pd
import requests
from bs4 import BeautifulSoup
from datetime import datetime


df = pd.DataFrame([], columns=['URL', 'Description',
                  'Qtd_Caracteres_Desscription', 'Title', 'Qtd_Caracteres_Title', 'Canonical'])

urls = ['https://empresas.ifood.com.br/ifood-beneficios']

getdate = datetime.now().strftime("%d-%m-%y")
name = input("Digite o nome do Cliente-> ")

for url in urls:
    res = requests.get(url)
    html_page = res.text
    soup = BeautifulSoup(html_page, 'html.parser')
    descricao = ""  # ok
    qtd_caracteres = 0  # ok
    title = ""
    qtd_caracteres_title = 0
    canonical = ""
    for meta in soup.find_all('meta'):
        if(meta.get('name') == 'description'):
            descricao = meta.get('content')
            qtd_caracteres = len(meta.get('content'))
    for titletag in soup.find_all('title'):
        title = titletag.get_text()
        qtd_caracteres_title = len(titletag.get_text())
    for link in soup.find_all('link'):
        if(link.get('rel') == "canonical"):
            canonical = link.get('href')
    dict = {'URL': url, 'Description': descricao,
            'Qtd_Caracteres_Desscription': qtd_caracteres, 'Title': title, 'Qtd_Caracteres_Title': qtd_caracteres_title, 'Canonical': canonical}
    df = df.append(dict, ignore_index=True)

df.to_csv(name + "_" + getdate + ".csv")
