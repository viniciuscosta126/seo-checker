import pandas as pd
import requests
from bs4 import BeautifulSoup
from datetime import datetime


df = pd.DataFrame([], columns=['Link Quebrado', 'Status', 'Texto'])

urls = ['https://blog-empresas.ifood.com.br/sistema-erp/',
        'https://blog-empresas.ifood.com.br/transformacao-digital-nas-empresas/']

getdate = datetime.now().strftime("%d-%m-%y")
name = input("Digite o nome do Cliente-> ")

for url in urls:
    res = requests.get(url)
    html_page = res.text
    soup = BeautifulSoup(html_page, 'html.parser')
    for link in soup.find_all('a'):
        if res.status_code != 200:
            dict = {'Link Quebrado': link.get(
                'href'), 'Status': res.status_code, "Texto": link.get_text()}
            df = df.append(dict, ignore_index=True)
    print(df)

df.to_csv(name + "_" + getdate + ".csv")
