import json
import os
import pandas as pd
from datetime import datetime
import time

"""Vamos armazenar as classificações de alto nível de cada URL em um dataframe. Vamos configurar esse dataframe vazio 
para usar mais tarde. Observe que existem dezenas de métricas que você pode extrair desses relatórios. Abra o arquivo de saída 
JSON depois de executar o Lighthouse para ver as possibilidades e, em seguida, adicione à coluna do dataframe abaixo e 
faça as edições necessárias quando pegarmos os dados do arquivo JSON."""

df = pd.DataFrame([], columns=['URL', 'SEO', 'Acessibilidade',
                  'Performance', 'Melhores Praticas', 'FCP', 'LCP', 'TBT', 'CLS', 'FID'])

"""Agora configuramos algumas variáveis ​fáceis que usaremos por toda parte. Usaremos o nome e getdate para nomear o arquivo 
de saída, e a lista de urls é o que vamos percorrer e executar o Lighthouse. Você tem duas opções para enviar o script à 
sua lista de URLs para verificar. Você pode usar o código e colocá-los em uma lista ou importar de um CSV, geralmente 
um Screaming Frog ou um arquivo de rastreamento semelhante."""

name = "RocketClicks"
getdate = datetime.now().strftime("%d-%m-%y")

urls = ["https://www.rocketclicks.com"]

"""Use este código abaixo se estiver importando de um arquivo de rastreamento. Altere YOUR_CRAWL_CSV para o caminho/nome
 do arquivo CSV de rastreamento. Em seguida, convertemos o dataframe em uma lista do Python."""

#df_urls = pd.read_csv("YOUR_CRAWL_CSV.csv")[["Address"]]
#urls = df_urls.values.tolist()

for url in urls:
    stream = os.popen('lighthouse --quiet --no-update-notifier --no-enable-error-reporting --output=json --output-path=YOUR_LOCAL_PATH' +
                      name+'_'+getdate+'.report.json --chrome-flags="--headless" ' + url)
    time.sleep(120)
    print("Report complete for: " + url)

    """Devido ao Python executar um aplicativo fora do script, precisamos pausar o script e aguardar a conclusão do Lighthouse. 
    Descobri que 2 minutos ou 120 segundos funcionam para a maioria das páginas. Ajuste conforme necessário se você receber um erro 
    informando que o arquivo de saída JSON não existe. A alternativa para uma pausa de script é escrever um loop procurando o arquivo 
    de saída e fazer com que ele volte indefinidamente até que o arquivo exista e então o script continue. Quando a pausa terminar 
    e o Lighthouse provavelmente terminar, construímos o caminho completo para o arquivo para que possamos processá-lo no próximo 
    snippet. Certifique-se de alterar “YOUR_LOCAL_PATH”."""

    json_filename = 'YOUR_LOCAL_PATH' + name + '_' + getdate + '.report.json'

    """Agora vamos abrir esse arquivo de relatório JSON e começar a processá-lo."""
    with open(json_filename, encoding='utf-8') as json_data:
        loaded_json = json.load(json_data)

        """Como mencionado anteriormente, há uma tonelada de dados neste arquivo de relatório e recomendo que você o examine e 
        escolha as coisas que deseja armazenar. Para este tutorial, vamos apenas pegar as classificações de alto nível para cada 
        uma das 4 categorias principais. Lembre-se de que essas pontuações são de 100. Multiplicamos por 100 aqui porque o 
        Lighthouse registra essas pontuações como floats de 0,00 a 1. 1 sendo 100% de pontuação."""

        seo = str(round(loaded_json["categories"]["seo"]["score"] * 100))
        accessibility = str(
            round(loaded_json["categories"]["accessibility"]["score"] * 100))
        performance = str(
            round(loaded_json["categories"]["performance"]["score"] * 100))
        best_practices = str(
            round(loaded_json["categories"]["best-practices"]["score"] * 100))

        fcp = loaded_json["audits"]['first-contentful-paint']['displayValue'].replace(' ','')
        lcp = loaded_json['audits']['largest-contentful-paint']['displayValue'].replace(' ','')
        tbt = loaded_json['audits']['total-blocking-time']['displayValue'].replace(' ','')
        layout_shift = loaded_json['audits']['cumulative-layout-shift']['displayValue'].replace(' ','')
        fid = loaded_json['audits']['max-potential-fid']['displayValue'].replace(' ','')
        """Agora pegamos essas classificações de alto nível e as colocamos em uma lista de dicionário e as anexamos ao dataframe. 
        Cada URL será adicionado a este dataframe. Depois disso, ele volta para a próxima URL, se houver uma"""

        dict = {"URL": url, "SEO": seo, "Acessibilidade": accessibility,
                "Performance": performance, "Melhores Praticas": best_practices, 'FCP': fcp, 'LCP': lcp, 'TBT': tbt, 'CLS': layout_shift, 'FID': fid}
        df = df.append(dict, ignore_index=True).sort_values(
            by='SEO', ascending=False)
        print(df)
"""Por fim, temos todas as nossas classificações no dataframe. A partir daqui, você pode fazer mais manipulação por conta própria,
canalizar para outro script ou armazenar em um banco de dados. Neste tutorial, vamos simplesmente exportar os dados para um 
arquivo CSV para ser aberto no Planilhas Google ou Excel. Certifique-se de substituir “SAVE_PATH”."""

df.to_csv('lighthouse_' + name + '_' + getdate + '.csv')
print(df)
