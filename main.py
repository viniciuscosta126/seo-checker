import requests
from bs4 import BeautifulSoup
import pandas as pd

itens_html = {
    'h1': 0,
    'h2': 0,
    'h3': 0,
    'h4': 0,
    'h5': 0,
    'h6': 0,
    'img': 0
}
df = pd.DataFrame([],['URL','H1','H2','H3','H4','H5','H6',"Imgs Sem Alt"])

def contar_tag(tag):
    itens_html[tag] = itens_html[tag] + 1


url = 'https://empresas.ifood.com.br/ifood-beneficios'
res = requests.get(url)
html_page = res.text

soup = BeautifulSoup(html_page, 'html.parser')


imagens_sem_alt = 0


for imgs in soup.find_all('img'):
    if(imgs.get('alt') == ""):
        contar_tag(imgs.name)

for i in range(1, 6, 1):
    for tag in soup.find_all(f'h{i}'):
        contar_tag(tag.name)
print(itens_html)
